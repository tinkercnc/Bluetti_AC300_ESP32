#ifndef CONFIG_H
#define CONFIG_H
#include "Arduino.h"

#define DEBUG                 1
#undef DEBUG

//#define RESET_WIFI_SETTINGS   1

#define EEPROM_SALT 13374

#define DEVICE_NAME "BLUETTI_AC300-MQTT"
/*
 "IP":"192.168.1.120", "MAC":"94:B9:7E:E6:39:A0" 
 */

#define BLUETOOTH_QUERY_MESSAGE_DELAY 4000  //orig 3000
#define DEVICE_STATE_UPDATE  1

#define RELAISMODE 1
#define RELAIS_PIN 22
#define RELAIS_LOW LOW
#define RELAIS_HIGH HIGH

#define MAX_DISCONNECTED_TIME_UNTIL_REBOOT 3 //device will reboot when wlan/BT/MQTT is not connectet within x Minutes

#ifndef BLUETTI_TYPE
  #define BLUETTI_TYPE BLUETTI_AC300
#endif


#endif
